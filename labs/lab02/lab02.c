//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

//library declaration
#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/float.h"
#include "pico/double.h"

//Settings of PI and the number of iterations
float pi_f = 3.14159265359;
double pi_d = 3.14159265359;
int iterations = 100000;

//Function Initialization
float wallis_pi_float(int n);
double wallis_pi_double(int n);

//Main function
int main() {

  #ifndef WOKWI
      // Initialise the IO as we will be using the UART
      // Only required for hardware and not needed for Wokwi
      stdio_init_all();
  #endif

  //Calculation of PI with single precision
  float pi_float = wallis_pi_float(iterations);
    //Calculation of the error
  float error_float = 100 * (pi_f - pi_float) / pi_f;
  printf("Approximate value of π after %d iterations using single precision: %0.12f \n", iterations, pi_float);
  printf("Approximate error is : %0.12f \n\n", error_float);

  //Calculation of PI with double precision
  double pi_double = wallis_pi_double(iterations);
    //Calculation of the error
  double error_double = 100 * (pi_d - pi_double) / pi_d;
  printf("Approximate value of π after %d iterations using double precision %0.12lf \n", iterations, pi_double);
  printf("Approximate error is : %0.12lf \n\n", error_double);

  return 0;
}

//Calculation of Pi with single precision
float wallis_pi_float(int n) {
  float pi = 2;
  float x = 0, y = 0;
  for (int i = 1; i <= n; i++) {
      float num = (2.0 * i) * (2.0 * i);
      float denom = ((2.0 * i) - 1) * ((2.0 * i) + 1);
      pi *= num / denom;
  } //End for loop
  return pi;
} //End of wallis_pi_float function

//Calculation of Pi with double precision
double wallis_pi_double(int n) {
  double pi = 2;
  for (int i = 1; i <= n; i++) {
      double num = (2.0 * i) * (2.0 * i);
      double denom = ((2.0 * i) - 1) * ((2.0 * i) + 1);
      pi *= num / denom;
  } //End for loop
  return pi;
} //End of wallis_pi_double function
